<?php

namespace Database\Factories;

use App\Models\BankAccount;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<BankAccount>
 */
class BankAccountFactory extends Factory
{

    /**
     * @inheritDoc
     */
    public function definition(): array
    {
        return [
            BankAccount::COLUMN_BALANCE        => $this->faker->numberBetween(100, null),
            BankAccount::COLUMN_ACCOUNT_NUMBER => $this->faker->randomNumber(8, true),
            BankAccount::COLUMN_USER_ID        => User::query()->pluck('id')[$this->faker->numberBetween(0, User::query()->count() - 1)],
        ];
    }
}
