<?php

namespace Database\Factories;

use App\Models\BankAccount;
use App\Models\BankCard;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<BankCard>
 */
class BankCardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            BankCard::COLUMN_ACCOUNT_ID  => BankAccount::query()->pluck('id')[$this->faker->numberBetween(1, BankAccount::query()->count() - 1)],
            BankCard::COLUMN_CARD_NUMBER => $this->faker->creditCardNumber($this->faker->creditCardType(), true),
        ];
    }
}
