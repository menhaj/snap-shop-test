<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends Factory<User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            User::COLUMN_NAME              => fake()->name(),
            User::COLUMN_EMAIL             => fake()->safeEmail(),
            User::COLUMN_EMAIL_VERIFIED_AT => now(),
            User::COLUMN_PASSWORD          => Hash::make('password'),
            User::COLUMN_REMEMBER_TOKEN    => Str::random(10),
            User::COLUMN_MOBILE            => '09333300025',
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return static
     */
    public function unverified(): static
    {
        return $this->state(function(array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
