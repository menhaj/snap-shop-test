<?php

namespace Database\Factories;

use App\Models\BankCard;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Transaction>
 */
class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            Transaction::COLUMN_AMOUNT       => $this->faker->numberBetween(10000, 500000000),
            Transaction::COLUMN_TO_CARD_ID   => BankCard::query()->pluck(BankCard::COLUMN_ID)[$this->faker->numberBetween(0, BankCard::query()->count() - 1)],
            Transaction::COLUMN_FROM_CARD_ID => BankCard::query()->pluck(BankCard::COLUMN_ID)[$this->faker->numberBetween(0, BankCard::query()->count() - 1)],
            Transaction::COLUMN_STATUS       => Transaction::STATUS_END,
            Transaction::COLUMN_CREATED_AT   => Carbon::now()->subMinutes(rand(1, 59)),
        ];
    }
}
