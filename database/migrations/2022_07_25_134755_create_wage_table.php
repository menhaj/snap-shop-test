<?php

use App\Models\Wage;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(Wage::getTableName(), function(Blueprint $table) {
            $table->id();
            $table->integer(Wage::COLUMN_TRANSACTION_ID);
            $table->integer(Wage::COLUMN_AMOUNT);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(Wage::getTableName());
    }
};
