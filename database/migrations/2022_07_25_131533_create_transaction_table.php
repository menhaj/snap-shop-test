<?php

use App\Models\Transaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(Transaction::getTableName(), function(Blueprint $table) {
            $table->id();
            $table->integer(Transaction::COLUMN_FROM_CARD_ID);
            $table->integer(Transaction::COLUMN_TO_CARD_ID);
            $table->integer(Transaction::COLUMN_AMOUNT);
            $table->enum(Transaction::COLUMN_STATUS, Transaction::STATUSES)
                  ->default(Transaction::STATUS_START);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(Transaction::getTableName());
    }
};
