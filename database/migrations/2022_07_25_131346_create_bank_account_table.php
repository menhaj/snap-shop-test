<?php

use App\Models\BankAccount;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(BankAccount::getTableName(), function(Blueprint $table) {
            $table->id();
            $table->integer(BankAccount::COLUMN_USER_ID);
            $table->integer(BankAccount::COLUMN_ACCOUNT_NUMBER);
            $table->bigInteger(BankAccount::COLUMN_BALANCE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(BankAccount::getTableName());
    }
};
