<?php

use App\Models\BankCard;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(BankCard::getTableName(), function(Blueprint $table) {
            $table->id();
            $table->integer(BankCard::COLUMN_ACCOUNT_ID);
            $table->string(BankCard::COLUMN_CARD_NUMBER);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(BankCard::getTableName());
    }
};
