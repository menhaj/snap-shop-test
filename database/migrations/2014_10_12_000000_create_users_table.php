<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(User::getTableName(), function(Blueprint $table) {
            $table->id();
            $table->string(User::COLUMN_NAME);
            $table->string(User::COLUMN_EMAIL)->unique();
            $table->timestamp(User::COLUMN_EMAIL_VERIFIED_AT)->nullable();
            $table->string(User::COLUMN_PASSWORD);
            $table->string(User::COLUMN_MOBILE);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(User::getTableName());
    }
};
