<?php

namespace App\Providers;

use App\Notifications\Providers\ISmsProvider;
use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        /**
         * Get default sms provider
         */
        $smsProvider = config('sms.providers.' . config('sms.default') . '.class');

        /**
         * check class exists
         */
        if(!empty($smsProvider) && class_exists($smsProvider) && (new $smsProvider) instanceof ISmsProvider)
        {
            /**
             * bind default sms provider class to laravel service container
             */
            $this->app->bind(ISmsProvider::class, $smsProvider);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
