<?php

namespace App\Providers;

use App\Interfaces\ITransactionRepository;
use App\Interfaces\IUserRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        /**
         * Bind transaction interface to its repository
         */
        $this->app->bind(ITransactionRepository::class, TransactionRepository::class);

        /**
         * Bind user interface to its repository
         */
        $this->app->bind(IUserRepository::class, UserRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
