<?php

namespace App\Exceptions;

class AccountBalanceNotEnoughException extends BaseException
{

    /**
     * @inheritDoc
     */
    protected function getErrorMessage(): string
    {
        return trans('account.balanceIsNotEnough');
    }

    /**
     * @inheritDoc
     */
    protected function getErrorCode(): int
    {
        return 500;
    }
}
