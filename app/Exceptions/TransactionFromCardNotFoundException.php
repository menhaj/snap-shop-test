<?php

namespace App\Exceptions;

class TransactionFromCardNotFoundException extends BaseException
{

	/**
	 * @inheritDoc
	 */
	protected function getErrorMessage(): string
	{
        return trans('transaction.fromCardNotFound');
	}

	/**
	 * @inheritDoc
	 */
	protected function getErrorCode(): int
	{
        return 404;
	}
}
