<?php

namespace App\Exceptions;


class BankCardNotFoundException extends BaseException
{
    /**
     * @return string
     */
    protected function getErrorMessage(): string
    {
        return trans('card.cardNotFound');
    }

    /**
     * @return int
     */
    protected function getErrorCode(): int
    {
        return 404;
    }
}
