<?php

namespace App\Exceptions;

use Exception;

abstract class BaseException extends Exception
{
    protected $message;

    protected $code;

    public function __construct()
    {
        parent::__construct($this->getErrorMessage(), $this->getErrorCode());
    }

    /**
     * @return string
     */
    abstract protected function getErrorMessage(): string;

    /**
     * @return int
     */
    abstract protected function getErrorCode(): int;
}
