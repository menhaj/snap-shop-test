<?php

namespace App\Exceptions;

class DestinationBankAccountNotFoundException extends BaseException
{
	/**
	 * @inheritDoc
	 */
	protected function getErrorMessage(): string
	{
        return trans('transaction.destinationBankAccountNotFound');
	}

	/**
	 * @inheritDoc
	 */
	protected function getErrorCode(): int
	{
        return 404;
	}
}
