<?php

namespace App\Exceptions;

use Exception;

class OriginCardNotFoundException extends BaseException
{
    //
    protected function getErrorMessage(): string
    {
        return trans('transaction.fromCardNotFound');
    }

    protected function getErrorCode(): int
    {
        return 404;
    }
}
