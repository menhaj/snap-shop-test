<?php

namespace App\Exceptions;

class TransactionToCardNotFoundException extends BaseException
{

	/**
	 * @inheritDoc
	 */
	protected function getErrorMessage(): string
	{
        return trans('transaction.toCardNotFound');
	}

	/**
	 * @inheritDoc
	 */
	protected function getErrorCode(): int
	{
        return 404;
	}
}
