<?php

namespace App\Exceptions;

class TransactionNotFoundException extends BaseException
{

	/**
	 * @inheritDoc
	 */
	protected function getErrorMessage(): string
	{
        return trans('transaction.notFound');
	}

	/**
	 * @inheritDoc
	 */
	protected function getErrorCode(): int
	{
        return 404;
	}
}
