<?php

namespace App\Exceptions;

class TransactionBetweenSameAccountException extends BaseException
{
    /**
     * @inheritDoc
     */
    protected function getErrorMessage(): string
    {
        return trans('transaction.sameAccountDetected');
    }

    /**
     * @inheritDoc
     */
    protected function getErrorCode(): int
    {
        return 500;
    }
}
