<?php

namespace App\Exceptions;

class OriginBankAccountNotFoundException extends BaseException
{
	/**
	 * @inheritDoc
	 */
	protected function getErrorMessage(): string
	{
        return trans('transaction.originBankAccountNotFound');
	}

	/**
	 * @inheritDoc
	 */
	protected function getErrorCode(): int
	{
        return 404;
	}
}
