<?php

namespace App\Http\Controllers;

use App\Http\Requests\MostTransactedUsersRequest;
use App\Interfaces\IUserRepository;
use Illuminate\Http\JsonResponse;

class UserController extends BaseController
{
    /**
     * @var IUserRepository
     */
    protected IUserRepository $userRepository;

    /**
     * @param IUserRepository $userRepository
     */
    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get most transacted users with their transactions
     *
     * @param MostTransactedUsersRequest $request
     *
     * @return JsonResponse
     */
    public function getUserMostTransacted(MostTransactedUsersRequest $request): JsonResponse
    {
        /**
         * result transactions count, by default its 10
         */
        $count = $request->get('count', 10);

        /**
         * Retrieve data from user repository
         */
        $result = $this->userRepository->getMostTransactedUsers($count);

        return $this->returnResult($result);
    }
}
