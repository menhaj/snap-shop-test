<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

class BaseController extends Controller
{
    const RESULT_STATUS_SUCCESS = 'success';
    const RESULT_STATUS_ERROR   = 'error';
    const RESULT_STATUS_WARNING = 'warning';
    const RESULT_STATUS_INFO    = 'info';


    /**
     * @param mixed|null  $data
     * @param string|null $message
     * @param string|null $status
     * @param int|null    $code
     *
     * @return JsonResponse
     */
    public function returnResult(
        mixed   $data = null,
        ?string $message = null,
        ?string $status = self::RESULT_STATUS_SUCCESS,
        ?int    $code = 200
    ): JsonResponse {

        $result = [
            'status' => $status,
        ];

        if(!is_null($message))
        {
            $result = array_merge($result, ['message' => $message]);
        }

        if(!is_null($data))
        {
            $result = array_merge($result, ['result' => $data]);
        }

        return response()->json(
            $result,
            $code
        );
    }
}
