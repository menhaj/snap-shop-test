<?php

namespace App\Http\Controllers;

use App\Exceptions\TransactionFromCardNotFoundException;
use App\Exceptions\TransactionToCardNotFoundException;
use App\Http\Requests\StoreTransactionRequest;
use App\Interfaces\ITransactionRepository;
use Illuminate\Http\JsonResponse;
use Throwable;

class TransactionController extends BaseController
{
    protected ITransactionRepository $transactionRepository;

    public function __construct(ITransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Store new transaction by an origin card and destination card
     *
     * @param StoreTransactionRequest $request
     *
     * @return JsonResponse
     * @throws TransactionFromCardNotFoundException
     * @throws TransactionToCardNotFoundException
     * @throws Throwable
     */
    public function store(StoreTransactionRequest $request)
    {
        /**
         * Process request in repository
         */
        $result = $this->transactionRepository->makeTransaction($request);

        /**
         * send response to client as json
         */
        return $this->returnResult($result, trans('transaction.createdSuccessfully'));
    }
}
