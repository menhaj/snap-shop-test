<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MostTransactedUsersRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'count' => 'nullable|numeric|min:1'
        ];
    }
}
