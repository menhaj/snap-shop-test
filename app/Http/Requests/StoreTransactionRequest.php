<?php

namespace App\Http\Requests;

use App\Rules\IsCardDigitsValid;
use Illuminate\Foundation\Http\FormRequest;

class StoreTransactionRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    const MIN_VALID_AMOUNT = 10000;
    const MAX_VALID_AMOUNT = 500000000;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'to_card'   => [
                'string',
                'required',
                'different:from_card',
                'regex:/^[0-9]{4}(?:-[0-9]{4}){3}$/u',
                new IsCardDigitsValid,
            ],
            'from_card' => [
                'string',
                'required',
                'regex:/^[0-9]{4}(?:-[0-9]{4}){3}$/u',
                new IsCardDigitsValid,
            ],
            'amount'    => [
                'required',
                'integer',
                'min:' . self::MIN_VALID_AMOUNT,
                'max:' . self::MAX_VALID_AMOUNT,
            ],
        ];
    }

    /**
     * Preparing request for validation
     *
     * @return void
     */
    protected function prepareForValidation(): void
    {
        $this->merge(
            [
                'from_card' => $this->convertNonEnglishDigitsToEnglish($this->get('from_card')),
                'to_card'   => $this->convertNonEnglishDigitsToEnglish($this->get('to_card')),
                'amount'    => $this->convertNonEnglishDigitsToEnglish($this->get('amount')),
            ]
        );
    }

    /**
     * @param string|int $string $string
     *
     * @return array|string|string[]
     */
    protected function convertNonEnglishDigitsToEnglish(string|int $string): array|string
    {
        $persianDigits1   = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $persianDigits2   = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١', '٠'];
        $allPersianDigits = array_merge($persianDigits1, $persianDigits2);
        $replaces         = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

        return str_replace($allPersianDigits, $replaces, $string);
    }
}
