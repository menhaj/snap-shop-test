<?php

namespace App\Interfaces;

use App\Exceptions\DestinationBankAccountNotFoundException;
use App\Exceptions\OriginBankAccountNotFoundException;
use App\Exceptions\TransactionBetweenSameAccountException;
use App\Exceptions\TransactionFromCardNotFoundException;
use App\Exceptions\TransactionToCardNotFoundException;
use App\Http\Requests\StoreTransactionRequest;
use App\Models\Transaction;
use Throwable;

interface ITransactionRepository
{
    /**
     * Make a transaction
     *
     * @param StoreTransactionRequest $request
     *
     * @return Transaction
     * @throws DestinationBankAccountNotFoundException
     * @throws OriginBankAccountNotFoundException
     * @throws Throwable
     * @throws TransactionBetweenSameAccountException
     * @throws TransactionFromCardNotFoundException
     * @throws TransactionToCardNotFoundException
     */
    public function makeTransaction(StoreTransactionRequest $request): Transaction;
}
