<?php

namespace App\Interfaces;

use Illuminate\Support\Collection;

interface IUserRepository
{
    /**
     * get most transacted users with their transactions
     *
     * @param int $count
     *
     * @return Collection
     */
    public function getMostTransactedUsers(int $count): Collection;
}
