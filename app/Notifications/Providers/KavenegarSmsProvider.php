<?php

namespace App\Notifications\Providers;

use App\Notifications\Messages\SmsMessage;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;

class KavenegarSmsProvider implements ISmsProvider
{
    /**
     * @param SmsMessage $message
     *
     * @return void
     * @throws RequestException
     */
    public function send(SmsMessage $message): void
    {
        $response = Http::get(
            $this->getSendEndpoint(),
            [
                'receptor' => $message->getTo(),
                'message'  => $message->getMessage(),
            ]
        );

        $response->throw();
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return config('sms.providers.kavenegar.api_key', '');
    }

    /**
     * @return string
     */
    public function getSendEndpoint(): string
    {
        return "https://api.kavenegar.com/v1/" . $this->getApiKey() . "/sms/send.json";
    }
}
