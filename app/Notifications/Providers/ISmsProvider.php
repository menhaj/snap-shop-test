<?php

namespace App\Notifications\Providers;

use App\Notifications\Messages\SmsMessage;
use Throwable;

interface ISmsProvider
{
    /**
     * @param SmsMessage $message
     *
     * @return void
     * @throws Throwable
     */
    public function send(SmsMessage $message): void;
}
