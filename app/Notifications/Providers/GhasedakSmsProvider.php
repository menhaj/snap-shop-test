<?php

namespace App\Notifications\Providers;

use App\Notifications\Messages\SmsMessage;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class GhasedakSmsProvider implements ISmsProvider
{
    /**
     * @param SmsMessage $message
     *
     * @return void
     * @throws RequestException
     */
    public function send(SmsMessage $message): void
    {
        Log::info(json_encode([$message->getMessage(), $message->getTo()]));

        $response = Http::withHeaders(
            [
                'apikey'       => $this->getApiKey(),
                'Accept'       => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'charset'      => 'utf-8',
            ]
        )->asForm()->post($this->getSendEndpoint(), [
            'message'  => $message->getMessage(),
            'receptor' => $message->getTo(),
        ]);

        $response->throw();
    }

    /**
     * @return string
     */
    protected function getApiKey(): string
    {
        return config('sms.providers.ghasedak.api_key', "");
    }

    /**
     * @return string
     */
    protected function getSendEndpoint(): string
    {
        return "https://api.ghasedak.me/v2/sms/send/simple?agent=php";
    }
}
