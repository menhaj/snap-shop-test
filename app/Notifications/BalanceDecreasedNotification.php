<?php

namespace App\Notifications;

use App\Exceptions\TransactionNotFoundException;
use App\Models\Transaction;

class BalanceDecreasedNotification extends SmsNotification
{
    protected int $transactionID;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(int $transactionID)
    {
        $this->transactionID = $transactionID;
    }

    /**
     * @return array
     * @throws TransactionNotFoundException
     */
    public function getTransactionData(): array
    {
        $transaction = Transaction::getByID($this->transactionID);

        if(!$transaction)
        {
            throw new TransactionNotFoundException();
        }

        return [
            'fromCard' => $transaction->fromCard->getCardNumber(),
            'toCard'   => $transaction->toCard->getCardNumber(),
            'amount'   => $transaction->getAmount(),
            'balance'  => $transaction->fromCard->account->getBalance(),
        ];
    }

    /**
     * @return string
     * @throws TransactionNotFoundException
     */
    protected function getMessage(): string
    {
        $transactionData = $this->getTransactionData();

        return trans(
            'notification.balanceDecreased',
            [
                'balance' => $transactionData['balance'],
                'amount' => $transactionData['amount'],
                'from_card' => $transactionData['fromCard'],
                'to_card' => $transactionData['toCard'],
            ]
        );
    }
}
