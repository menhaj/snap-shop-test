<?php

namespace App\Notifications;

use App\Models\User;
use App\Notifications\Channels\SmsChannel;
use App\Notifications\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

abstract class SmsNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    final public function via(mixed $notifiable): array
    {
        return [SmsChannel::class];
    }

    /**
     * @param User $notifiable
     *
     * @return SmsMessage
     */
    final public function toSms(User $notifiable): SmsMessage
    {
        /**
         * Get transaction data
         */
        $message = $this->getMessage();

        return (new SmsMessage())
            ->setMessage($message)
            ->setTo($notifiable->getMobile());
    }

    /**
     * Get message of sms notification
     *
     * @return string
     */
    abstract protected function getMessage(): string;
}
