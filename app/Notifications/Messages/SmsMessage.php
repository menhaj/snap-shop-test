<?php

namespace App\Notifications\Messages;

class SmsMessage
{
    protected string     $message = "";
    protected string|int $to;

    /**
     * @param string|int $to
     *
     * @return SmsMessage
     */
    public function setTo(string|int $to): SmsMessage
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @return int|string
     */
    public function getTo(): int|string
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return SmsMessage
     */
    public function setMessage(string $message): SmsMessage
    {
        $this->message = $message;

        return $this;
    }
}
