<?php

namespace App\Notifications\Channels;

use App\Models\User;
use App\Notifications\Messages\SmsMessage;
use App\Notifications\Providers\ISmsProvider;
use App\Notifications\SmsNotification;
use Illuminate\Support\Facades\Log;
use Throwable;

class SmsChannel
{
    /**
     * @var ISmsProvider
     */
    protected ISmsProvider $smsProvider;

    /**
     * @param ISmsProvider $smsProvider
     */
    public function __construct(ISmsProvider $smsProvider)
    {
        $this->smsProvider = $smsProvider;
    }

    /**
     * @param User            $notifiable
     * @param SmsNotification $notification
     *
     * @return void
     * @throws Throwable
     */
    public function send(User $notifiable, SmsNotification $notification): void
    {
        /**
         * Message
         */
        $message = $notification->toSms($notifiable);

        /**
         * send by provider
         */
        $this->sendByProvider($message);
    }

    /**
     * @param SmsMessage $message
     *
     * @return void
     */
    protected function sendByProvider(SmsMessage $message): void
    {
        try
        {
            /**
             * send message by default provider
             */
            $this->smsProvider->send($message);
        }
        catch(Throwable $ex)
        {
            Log::error($ex->getMessage(), $ex->getTrace());
        }
    }
}
