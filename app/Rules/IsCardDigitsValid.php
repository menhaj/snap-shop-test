<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsCardDigitsValid implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->isCardDigitsValid($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('transaction.cardNumberFormatIsInvalid');
    }


    /**
     * @param string $card
     *
     * @return bool
     */
    protected function isCardDigitsValid(string $card): bool
    {
        $card = (string)preg_replace('/\D/', '', $card);

        $strlen = strlen($card);

        if($strlen != 16 || !in_array($card[0], [2, 4, 5, 6, 9]))
        {
            return false;
        }

        $res = null;

        for($i = 0; $i < $strlen; $i++)
        {
            $res[$i] = $card[$i];
            if(($strlen % 2) == ($i % 2))
            {
                $res[$i] *= 2;
                if($res[$i] > 9)
                {
                    $res[$i] -= 9;
                }
            }
        }

        return array_sum($res) % 10 == 0;
    }
}
