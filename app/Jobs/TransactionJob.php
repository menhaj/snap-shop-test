<?php

namespace App\Jobs;

use App\Exceptions\AccountBalanceNotEnoughException;
use App\Models\BankAccount;
use App\Models\Transaction;
use App\Models\Wage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class TransactionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Transaction ID
     *
     * @var int
     */
    protected int $transactionID;

    /**
     * @var Transaction
     */
    protected Transaction $transaction;

    /**
     * @param int $transactionID
     */
    public function __construct(int $transactionID)
    {
        $this->transactionID = $transactionID;
    }

    /**
     * Execute Transaction job.
     *
     * @return void
     * @throws Throwable
     */
    public function handle(): void
    {
        /**
         * Beginning DB transaction
         */
        DB::beginTransaction();

        /**
         * Get fresh transaction from db
         *
         * @var Transaction $transaction
         */
        $transaction = Transaction::query()->find($this->transactionID);

        /**
         * set class transaction
         */
        $this->transaction = $transaction;

        try
        {
            /**
             * Set transaction status as process
             */
            $this->setTransactionStatus(Transaction::STATUS_PROCESS);

            /**
             * Calculate total transaction amount
             */
            $transactionAmount = $this->calculateTransactionAmount();

            /**
             * Handle origin account
             */
            $this->handleOriginAccount($transactionAmount);

            /**
             * Handle origin account
             */
            $this->handleDestinationAccount($transactionAmount);

            /**
             * Collecting wage
             */
            $this->collectWage();

            /**
             * Set transaction status as end
             */
            $this->setTransactionStatus(Transaction::STATUS_END);

            /**
             * Commit the changes to DB
             */
            DB::commit();
        }
        catch(Throwable $ex)
        {
            /**
             * On job fails then rolling back DB
             */
            DB::rollBack();

            /**
             * Set transaction status as failed
             */
            $this->setTransactionStatus(Transaction::STATUS_FAILED);

            /**
             * Log the error
             */
            Log::error($ex->getMessage(), $ex->getTrace());
        }
        finally
        {
            switch($this->transaction->getStatus())
            {
                case Transaction::STATUS_END:
                    $this->sendSuccessfulNotification();
                    break;

                case Transaction::STATUS_FAILED:
                    $this->sendTransactionFailedNotification();
                    break;
            }
        }
    }

    /**
     * collecting wage
     *
     * @return void
     * @throws Throwable
     */
    protected function collectWage(): void
    {
        $params = [
            Wage::COLUMN_TRANSACTION_ID => $this->transactionID,
            Wage::COLUMN_AMOUNT         => Wage::WAGE_AMOUNT,
        ];

        Wage::add($params);
    }

    /**
     * Set transaction status
     *
     * @param string $status
     *
     * @return void
     * @throws Throwable
     */
    protected function setTransactionStatus(string $status): void
    {
        $this->transaction->setStatus($status)->save();
    }

    /**
     * @param int $amount
     *
     * @return void
     * @throws AccountBalanceNotEnoughException
     * @throws Throwable
     */
    protected function handleOriginAccount(int $amount): void
    {
        /**
         * Get origin bank account and lock its row to update
         *
         * @var BankAccount $fromAccount
         */
        $fromAccount = BankAccount::query()->lockForUpdate()->find($this->transaction->fromCard->getAccountID());

        /**
         * Compare balance and amount
         */
        if($fromAccount->getBalance() < $this->transaction->getAmount())
        {
            throw new AccountBalanceNotEnoughException();
        }

        /**
         * Calculate origin account new balance
         */
        $fromAccountNewBalance = $fromAccount->getBalance() - $amount;

        /**
         * Save origin account new balance
         */
        $fromAccount->setBalance($fromAccountNewBalance)->save();
    }

    /**
     * @param int $amount
     *
     * @return void
     * @throws Throwable
     */
    protected function handleDestinationAccount(int $amount): void
    {
        /**
         * Get destination bank account and lock its row to update
         *
         * @var BankAccount $toAccount
         */
        $toAccount = BankAccount::query()->lockForUpdate()->find($this->transaction->toCard->getAccountID());

        /**
         * Calculate destination account new balance
         */
        $toAccountNewBalance = $toAccount->getBalance() + $amount;

        /**
         * Save destination account new balance
         */
        $toAccount->setBalance($toAccountNewBalance)->save();
    }

    /**
     * @return int
     */
    protected function calculateTransactionAmount(): int
    {
        return $this->transaction->getAmount() + Wage::WAGE_AMOUNT;
    }

    /**
     * Send notification to origin and destination users
     *
     * @return void
     */
    protected function sendSuccessfulNotification(): void
    {
        /**
         * Retrieve origin user
         */
        $fromUser = $this->transaction->fromCard->account->user;

        /**
         * Retrieve destination user
         */
        $toUser = $this->transaction->fromCard->account->user;

        /**
         * Send decreasing balance sms notification to origin user
         */
        $fromUser->sendBalanceDecreasedNotification($this->transactionID);

        /**
         * Send increasing balance sms notification to destination user
         */
        $toUser->sendBalanceIncreasedNotification($this->transactionID);
    }

    /**
     * Send transaction failed to origin user
     *
     * @return void
     */
    protected function sendTransactionFailedNotification(): void
    {
        /**
         * Retrieve origin user
         */
        $fromUser = $this->transaction->fromCard->account->user;

        /**
         * Send balance not enough sms notification to destination user
         */
        $fromUser->sendTransactionFailedNotification($this->transactionID);
    }
}
