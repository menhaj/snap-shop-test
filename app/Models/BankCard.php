<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Cache;

/**
 * @property BankAccount account
 */
class BankCard extends BaseModel
{
    use HasFactory;

    protected $table = 'bank_cards';

    const COLUMN_ACCOUNT_ID = 'account_id';
    const COLUMN_CARD_NUMBER = 'card_number';

    protected $fillable = [
        self::COLUMN_ACCOUNT_ID,
        self::COLUMN_CARD_NUMBER,
    ];

    /**
     * @return BelongsTo
     */
    public function account(): BelongsTo
    {
        return $this->belongsTo(BankAccount::class, self::COLUMN_ACCOUNT_ID);
    }

    /**
     * @return int
     */
    public function getAccountID(): int
    {
        return $this->{self::COLUMN_ACCOUNT_ID};
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setAccountID(int $value): BankCard
    {
        $this->{self::COLUMN_ACCOUNT_ID} = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getCardNumber(): string
    {
        return $this->{self::COLUMN_CARD_NUMBER};
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setCardNumber(int $value): BankCard
    {
        $this->{self::COLUMN_CARD_NUMBER} = $value;

        return $this;
    }

    /**
     * @param string $cardNumber
     *
     * @return BankCard|Builder|Builder[]|Collection|Model|null
     */
    public static function getByCardNumber(string $cardNumber): Model|Collection|BankCard|Builder|array|null
    {
        return Cache::remember(
            self::getCacheKey($cardNumber),
            self::CACHE_TTL,
            function() use ($cardNumber) {
                return static::query()->where(self::COLUMN_CARD_NUMBER, $cardNumber)->first();
            }
        );
    }
}
