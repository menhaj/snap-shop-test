<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Throwable;

/**
 * @property BankCard fromCard
 * @property BankCard toCard
 */
class Transaction extends BaseModel
{
    use HasFactory;

    protected $table = 'transactions';

    const COLUMN_FROM_CARD_ID = 'from_card_id';
    const COLUMN_TO_CARD_ID   = 'to_card_id';
    const COLUMN_AMOUNT       = 'amount';
    const COLUMN_STATUS       = 'status';

    const STATUS_START   = 'start';
    const STATUS_PROCESS = 'process';
    const STATUS_END     = 'end';
    const STATUS_FAILED  = 'failed';

    const STATUSES = [
        self::STATUS_START   => self::STATUS_START,
        self::STATUS_PROCESS => self::STATUS_PROCESS,
        self::STATUS_END     => self::STATUS_END,
        self::STATUS_FAILED  => self::STATUS_FAILED,
    ];

    protected $fillable = [
        self::COLUMN_FROM_CARD_ID,
        self::COLUMN_TO_CARD_ID,
        self::COLUMN_AMOUNT,
    ];

    /**
     * @return BelongsTo
     */
    public function fromCard(): BelongsTo
    {
        return $this->belongsTo(BankCard::class, self::COLUMN_FROM_CARD_ID);
    }

    /**
     * @return BelongsTo
     */
    public function toCard(): BelongsTo
    {
        return $this->belongsTo(BankCard::class, self::COLUMN_TO_CARD_ID);
    }

    /**
     * @return int
     */
    public function getFromCardID(): int
    {
        return $this->{self::COLUMN_FROM_CARD_ID};
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setFromCardID(int $value): Transaction
    {
        $this->{self::COLUMN_FROM_CARD_ID} = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getToCardID(): int
    {
        return $this->{self::COLUMN_TO_CARD_ID};
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setToCardID(int $value): Transaction
    {
        $this->{self::COLUMN_TO_CARD_ID} = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->{self::COLUMN_AMOUNT};
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setAmount(int $value): Transaction
    {
        $this->{self::COLUMN_AMOUNT} = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->{self::COLUMN_STATUS};
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setStatus(string $value): Transaction
    {
        $this->{self::COLUMN_STATUS} = $value;

        return $this;
    }

    /**
     * @param array $params
     *
     * @return Transaction
     * @throws Throwable
     */
    public static function add(array $params): Transaction
    {
        $validation = Validator::make($params, [
            self::COLUMN_FROM_CARD_ID => 'numeric|required',
            self::COLUMN_TO_CARD_ID   => 'numeric|required|different:' . self::COLUMN_FROM_CARD_ID,
            self::COLUMN_AMOUNT       => 'numeric|required|gt:0',
            self::COLUMN_STATUS       => ['string', 'required', Rule::in(self::STATUSES)],
        ])->validate();

        $transaction = new Transaction();
        $transaction->setFromCardID($validation[self::COLUMN_FROM_CARD_ID])
                    ->setToCardID($validation[self::COLUMN_TO_CARD_ID])
                    ->setAmount($validation[self::COLUMN_AMOUNT])
                    ->setStatus($validation[self::COLUMN_STATUS])
                    ->save();

        return $transaction;
    }

    /**
     * Get last transactions of a user by his ID
     *
     * @param int $userID
     * @param int $count
     *
     * @return Collection
     */
    public static function getLastTransactionsByUserID(int $userID, int $count): Collection
    {
        return Transaction::query()
                          ->join(
                              BankCard::getTableName('originCard'),
                              Transaction::getColumn(Transaction::COLUMN_FROM_CARD_ID),
                              '=',
                              BankCard::getColumn(BankCard::COLUMN_ID, true, 'originCard')
                          )
                          ->join(
                              BankCard::getTableName('destinationCard'),
                              Transaction::getColumn(Transaction::COLUMN_TO_CARD_ID),
                              '=',
                              BankCard::getColumn(BankCard::COLUMN_ID, true, 'destinationCard')
                          )
                          ->join(
                              BankAccount::getTableName('originAccount'),
                              BankCard::getColumn(BankCard::COLUMN_ACCOUNT_ID, true, 'originCard'),
                              '=',
                              BankAccount::getColumn(BankAccount::COLUMN_ID, true, 'originAccount')
                          )
                          ->join(
                              BankAccount::getTableName('destinationAccount'),
                              BankCard::getColumn(BankCard::COLUMN_ACCOUNT_ID, true, 'destinationCard'),
                              '=',
                              BankAccount::getColumn(BankAccount::COLUMN_ID, true, 'destinationAccount')
                          )
                          ->join(
                              User::getTableName('originUser'),
                              BankAccount::getColumn(BankAccount::COLUMN_USER_ID, true, 'originAccount'),
                              '=',
                              User::getColumn(User::COLUMN_ID, true, 'originUser')
                          )
                          ->join(
                              User::getTableName('destinationUser'),
                              BankAccount::getColumn(BankAccount::COLUMN_USER_ID, true, 'destinationAccount'),
                              '=',
                              User::getColumn(User::COLUMN_ID, true, 'destinationUser')
                          )
                          ->where(User::getColumn(User::COLUMN_ID, true, 'originUser'), '=', $userID)
                          ->orWhere(User::getColumn(User::COLUMN_ID, true, 'destinationUser'), '=', $userID)
                          ->select(
                              [
                                  Transaction::getColumn('*'),
                                  BankCard::getColumn(BankCard::COLUMN_CARD_NUMBER, true, 'destinationCard') . ' as toCard',
                                  BankCard::getColumn(BankCard::COLUMN_CARD_NUMBER, true, 'originCard') . ' as fromCard',
                              ]
                          )
                          ->orderByDesc(Transaction::getColumn(Transaction::CREATED_AT))
                          ->limit($count)
                          ->get();
    }
}
