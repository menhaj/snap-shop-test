<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

abstract class BaseModel extends Model
{
    const CACHE_TTL = 86400;

    const COLUMN_ID         = 'id';
    const COLUMN_CREATED_AT = 'created_at';

    /**
     * Save method
     *
     * @param array $options
     *
     * @return bool
     * @throws Throwable
     */
    public function save(array $options = []): bool
    {
        /**
         * Beginning transaction
         */
        DB::beginTransaction();

        try
        {
            /**
             * Call parent save method
             */
            $result = parent::save($options);

            /**
             * Commit the changes
             */
            DB::commit();

            /**
             * return result
             */
            return $result;
        }
        catch(Throwable $exception)
        {
            /**
             * Log error
             */
            Log::error($exception->getMessage(), $exception->getTrace());

            /**
             * Roll back changes
             */
            DB::rollBack();

            /**
             * throw happened exception
             */
            throw $exception;
        }
    }

    /**
     * Get by ID method
     *
     * @param int $id
     *
     * @return $this|null
     */
    public static function getByID(int $id)
    {
        return Cache::remember(static::getCacheKey($id), self::CACHE_TTL, function() use ($id) {
            $calledModel = get_called_class();

            /**
             * @var BaseModel $calledModelInstance
             */
            $calledModelInstance = (new $calledModel);

            $object = $calledModelInstance->query()->find($id);

            if($object)
            {
                return $object;
            }

            return null;
        });
    }

    /**
     * @param string|int $key
     *
     * @return string
     */
    protected static function getCacheKey(string|int $key): string
    {
        return self::getTableName() . ':' . $key;
    }

    /**
     * Return ID of row data
     *
     * @return int
     */
    public function getID(): int
    {
        return $this->{self::COLUMN_ID};
    }

    /**
     * get model column name with model table
     *
     * @param string      $columnName
     * @param bool        $hasTable
     * @param string|null $tableName
     *
     * @return string
     */
    public static function getColumn(string $columnName, bool $hasTable = true, ?string $tableName = null): string
    {
        $result = '';

        if($hasTable && !$tableName)
        {
            $result .= self::getTableName() . '.';
        }

        if($hasTable && !!$tableName)
        {
            $result .= $tableName . '.';
        }

        return $result . $columnName;
    }

    /**
     * get model table name
     *
     * @param string|null $as
     *
     * @return string
     */
    public static function getTableName(?string $as = ''): string
    {
        $calledModel = get_called_class();

        /**
         * @var BaseModel $model
         */
        $model = (new $calledModel);

        $table = $model->getTable();

        if($as)
        {
            $table .= ' as ' . $as;
        }

        return $table;
    }
}
