<?php

namespace App\Models;

use App\Notifications\BalanceDecreasedNotification;
use App\Notifications\BalanceIncreasedNotification;
use App\Notifications\BalanceNotEnoughNotification;
use App\Notifications\TransactionFailedNotification;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use HasApiTokens, HasFactory, Notifiable, Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail;

    protected $table = 'users';

    const COLUMN_NAME              = 'name';
    const COLUMN_EMAIL             = 'email';
    const COLUMN_PASSWORD          = 'password';
    const COLUMN_MOBILE            = 'mobile';
    const COLUMN_REMEMBER_TOKEN    = 'remember_token';
    const COLUMN_EMAIL_VERIFIED_AT = 'email_verified_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        self::COLUMN_NAME,
        self::COLUMN_EMAIL,
        self::COLUMN_PASSWORD,
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        self::COLUMN_PASSWORD,
        self::COLUMN_REMEMBER_TOKEN,
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        self::COLUMN_EMAIL_VERIFIED_AT => 'datetime',
    ];

    /**
     * @param int $transactionID
     *
     * @return void
     */
    public function sendBalanceIncreasedNotification(int $transactionID): void
    {
        $this->notify(new BalanceIncreasedNotification($transactionID));
    }

    /**
     * @param int $transactionID
     *
     * @return void
     */
    public function sendBalanceDecreasedNotification(int $transactionID): void
    {
        $this->notify(new BalanceDecreasedNotification($transactionID));
    }

    /**
     * @param int $transactionID
     *
     * @return void
     */
    public function sendTransactionFailedNotification(int $transactionID): void
    {
        $this->notify(new TransactionFailedNotification($transactionID));
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->{self::COLUMN_NAME};
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setName(string $value): User
    {
        $this->{self::COLUMN_NAME} = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->{self::COLUMN_EMAIL};
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setEmail(string $value): User
    {
        $this->{self::COLUMN_EMAIL} = $value;

        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setPassword(string $value): User
    {
        $this->{self::COLUMN_NAME} = Hash::make($value);

        return $this;
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->{self::COLUMN_MOBILE};
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setMobile(string $value): User
    {
        $this->{self::COLUMN_MOBILE} = $value;

        return $this;
    }

    /**
     * @return Collection
     */
    public static function getMostTransactedUsers(): Collection
    {
        return User::query()
                   ->join(
                       BankAccount::getTableName(),
                       User::getColumn(User::COLUMN_ID),
                       '=',
                       BankAccount::getColumn(BankAccount::COLUMN_USER_ID)
                   )
                   ->join(
                       BankCard::getTableName(),
                       BankAccount::getColumn(BankAccount::COLUMN_ID),
                       '=',
                       BankCard::getColumn(BankCard::COLUMN_ACCOUNT_ID)
                   )
                   ->join(
                       Transaction::getTableName('FromTransaction'),
                       BankCard::getColumn(BankCard::COLUMN_ID),
                       '=',
                       'FromTransaction.' . Transaction::COLUMN_FROM_CARD_ID
                   )
                   ->join(
                       Transaction::getTableName('ToTransaction'),
                       BankCard::getColumn(BankCard::COLUMN_ID),
                       '=',
                       'ToTransaction.' . Transaction::COLUMN_TO_CARD_ID
                   )
                   ->where('FromTransaction.' . Transaction::COLUMN_CREATED_AT, '<', Carbon::now()->subMinutes(10))
                   ->orWhere('ToTransaction.' . Transaction::COLUMN_CREATED_AT, '<', Carbon::now()->subMinutes(10))
                   ->select([User::getColumn(User::COLUMN_ID), DB::raw('count(*) as total')])
                   ->groupBy(User::getColumn(User::COLUMN_ID))
                   ->orderByDesc('total')
                   ->limit(3)
                   ->get();
    }
}
