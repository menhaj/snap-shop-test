<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Validator;
use Throwable;

class Wage extends BaseModel
{
    use HasFactory;

    protected $table = 'wages';

    const COLUMN_TRANSACTION_ID = 'transaction_id';
    const COLUMN_AMOUNT         = 'amount';

    const WAGE_AMOUNT = 5000;

    protected $fillable = [
        self::COLUMN_TRANSACTION_ID,
        self::COLUMN_AMOUNT,
    ];

    /**
     * @return BelongsTo
     */
    public function transaction(): BelongsTo
    {
        return $this->belongsTo(Transaction::class, self::COLUMN_TRANSACTION_ID);
    }

    /**
     * @return int
     */
    public function getTransactionID(): int
    {
        return $this->{self::COLUMN_TRANSACTION_ID};
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setTransactionID(int $value): Wage
    {
        $this->{self::COLUMN_TRANSACTION_ID} = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->{self::COLUMN_AMOUNT};
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setAmount(int $value): Wage
    {
        $this->{self::COLUMN_AMOUNT} = $value;

        return $this;
    }

    /**
     * @param array $params
     *
     * @return Wage
     * @throws Throwable
     */
    public static function add(array $params): Wage
    {
        $validation = Validator::make($params, [
            self::COLUMN_TRANSACTION_ID => 'integer|required',
            self::COLUMN_AMOUNT         => 'integer|required',
        ])->validate();

        $wage = new Wage();

        $wage->setTransactionID($validation[self::COLUMN_TRANSACTION_ID])
             ->setAmount($validation[self::COLUMN_AMOUNT])
             ->save();

        return $wage;
    }
}
