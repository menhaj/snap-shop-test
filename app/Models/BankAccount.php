<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property User user
 */
class BankAccount extends BaseModel
{
    use HasFactory;

    protected $table = 'bank_accounts';

    const COLUMN_ACCOUNT_NUMBER = 'account_number';
    const COLUMN_USER_ID        = 'user_id';
    const COLUMN_BALANCE        = 'balance';

    protected $fillable = [
        self::COLUMN_ACCOUNT_NUMBER,
        self::COLUMN_USER_ID,
        self::COLUMN_BALANCE,
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, self::COLUMN_USER_ID);
    }

    /**
     * @return int
     */
    public function getAccountNumber(): int
    {
        return $this->{self::COLUMN_ACCOUNT_NUMBER};
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setAccountNumber(int $value): BankAccount
    {
        $this->{self::COLUMN_ACCOUNT_NUMBER} = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserID(): int
    {
        return $this->{self::COLUMN_USER_ID};
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setUserID(int $value): BankAccount
    {
        $this->{self::COLUMN_USER_ID} = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getBalance(): int
    {
        return $this->{self::COLUMN_BALANCE};
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setBalance(int $value): BankAccount
    {
        $this->{self::COLUMN_BALANCE} = $value;

        return $this;
    }

}
