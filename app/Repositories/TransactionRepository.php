<?php

namespace App\Repositories;

use App\Exceptions\AccountBalanceNotEnoughException;
use App\Exceptions\DestinationBankAccountNotFoundException;
use App\Exceptions\OriginBankAccountNotFoundException;
use App\Exceptions\TransactionBetweenSameAccountException;
use App\Exceptions\TransactionFromCardNotFoundException;
use App\Exceptions\TransactionToCardNotFoundException;
use App\Http\Requests\StoreTransactionRequest;
use App\Interfaces\ITransactionRepository;
use App\Jobs\TransactionJob;
use App\Models\BankCard;
use App\Models\Transaction;
use Throwable;

class TransactionRepository implements ITransactionRepository
{
    /**
     * Make a transaction
     *
     * @param StoreTransactionRequest $request
     *
     * @return Transaction
     * @throws DestinationBankAccountNotFoundException
     * @throws OriginBankAccountNotFoundException
     * @throws Throwable
     * @throws TransactionBetweenSameAccountException
     * @throws TransactionFromCardNotFoundException
     * @throws TransactionToCardNotFoundException
     */
    public function makeTransaction(StoreTransactionRequest $request): Transaction
    {
        /**
         * Retrieve parameters from validator
         */
        $fromCardNumber = $request->get('from_card');
        $toCardNumber   = $request->get('to_card');
        $amount         = $request->get('amount');

        /**
         * Find origin bank card by its number
         */
        $fromCard = BankCard::getByCardNumber($fromCardNumber);

        /**
         * If origin bank card not found then throw its exception
         */
        if(!$fromCard)
        {
            throw new TransactionFromCardNotFoundException();
        }

        /**
         * Find destination bank card by its number
         */
        $toCard = BankCard::getByCardNumber($toCardNumber);

        /**
         * If destination bank card not found then throw its exception
         */
        if(!$toCard)
        {
            throw new TransactionToCardNotFoundException();
        }

        /**
         * Validate cards
         */
        $this->_validateTransactionCards($fromCard, $toCard, $amount);

        /**
         * Create params array to pass into Transaction model for create new transaction record
         */
        $params = [
            Transaction::COLUMN_AMOUNT       => $amount,
            Transaction::COLUMN_TO_CARD_ID   => $toCard->getID(),
            Transaction::COLUMN_FROM_CARD_ID => $fromCard->getID(),
            Transaction::COLUMN_STATUS       => Transaction::STATUS_START,
        ];

        /**
         * Add new record to transaction table
         */
        $transaction = Transaction::add($params);

        /**
         * Dispatch transaction job
         */
        TransactionJob::dispatch($transaction->getID());

        return $transaction;
    }

    /**
     * Transaction cards validation
     *
     * @param BankCard $fromCard
     * @param BankCard $toCard
     * @param int      $amount
     *
     * @return void
     * @throws DestinationBankAccountNotFoundException
     * @throws OriginBankAccountNotFoundException
     * @throws TransactionBetweenSameAccountException
     * @throws AccountBalanceNotEnoughException
     */
    protected function _validateTransactionCards(BankCard $fromCard, BankCard $toCard, int $amount): void
    {
        /**
         * if origin account is not found then return exception
         */
        if(!isset($fromCard->account))
        {
            throw new OriginBankAccountNotFoundException();
        }

        /**
         * if destination account is not found then return exception
         */
        if(!isset($toCard->account))
        {
            throw new DestinationBankAccountNotFoundException();
        }

        /**
         * if origin card and destination card belong to same account then return exception
         */
        if($fromCard->getAccountID() == $toCard->getAccountID())
        {
            throw new TransactionBetweenSameAccountException();
        }

        /**
         * check origin account balance
         */
        if($fromCard->account->getBalance() < $amount)
        {
            throw new AccountBalanceNotEnoughException();
        }
    }
}
