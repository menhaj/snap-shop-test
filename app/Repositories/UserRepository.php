<?php

namespace App\Repositories;

use App\Interfaces\IUserRepository;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Collection;

class UserRepository implements IUserRepository
{
    /**
     * Returns most transacted user
     *
     * @param int $count
     *
     * @return Collection
     */
    public function getMostTransactedUsers(int $count): Collection
    {
        return User::getMostTransactedUsers()->map(
            function($result) use ($count) {

                /**
                 * Get user id
                 */
                $userID = $result[User::COLUMN_ID];

                /**
                 * get user object
                 */
                $user = User::getByID($userID);

                /**
                 * add user total transaction count to its object
                 */
                $user['total_transactions'] = $result['total'];

                /**
                 * add user transactions to its object
                 */
                $user['transactions'] = Transaction::getLastTransactionsByUserID($userID, $count);

                return $user;
            }
        );
    }
}
