<?php

return [
    'balanceDecreased'  => 'Transfer succeed.<br>Account balance: :balance Rials.<br>Amount: :amount Rials<br>From: :from_card<br>To: :to_card',
    'balanceIncreased'  => 'Deposit received.<br>Account balance: :balance Rials.<br>Amount: :amount Rials<br>From: :from_card<br>To: :to_card',
    'balanceNotEnough'  => 'Transfer failed.<br>Account balance is not enough.<br>Account balance: :balance Rials.<br>Amount: :amount Rials<br>From: :from_card<br>To: :to_card',
    'transactionFailed' => 'Transfer failed.<br>Account balance: :balance Rials.<br>Amount: :amount Rials<br>From: :from_card<br>To: :to_card',
];
