<?php

return [
    'fromCardNotFound'               => 'The origin card not found.',
    'toCardNotFound'                 => 'The destination card not found.',
    'addMissionFailed'               => 'The transaction add mission has failed.',
    'createdSuccessfully'            => 'The transaction created successfully.',
    'cardNumberFormatIsInvalid'      => ':attribute number is invalid.',
    'notFound'                       => 'Transaction not found.',
    'originBankAccountNotFound'      => 'Origin bank account not found.',
    'destinationBankAccountNotFound' => 'Destination bank account not found.',
    'sameAccountDetected'            => 'Transaction between same account cards is not valid.',
];
