<?php

return [
    /**
     * Default sms provider
     */
    'default'   => env('SMS_PROVIDER', 'kavenegar'),

    /**
     * List of sms providers
     */
    'providers' => [
        /**
         * Kavenegar sms provider
         */
        'kavenegar' => [
            "class"   => \App\Notifications\Providers\KavenegarSmsProvider::class,
            "api_key" => env('KAVENEGAR_SMS_PROVIDER_API_KEY', ''),
        ],
        /**
         * Ghasedak sms provider
         */
        'ghasedak'  => [
            "class"   => \App\Notifications\Providers\GhasedakSmsProvider::class,
            "api_key" => env("GHASEDAK_SMS_PROVIDER_API_KEY", ""),
        ],
    ],
];
