<?php

use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function(Request $request) {
    return $request->user();
});

/**
 * Transaction API
 */
Route::prefix('transaction')->controller(TransactionController::class)->group(function() {
    /**
     * Storing new transaction
     */
    Route::post('/', 'store')->name('api.transaction.store');
});

/**
 * User API
 */
Route::prefix('user')->controller(UserController::class)->group(function() {
    /**
     * get 3 users who most transacted in last 10 minutes
     */
    Route::get('/most-transacted', 'getUserMostTransacted')->name('api.user.mostTransacted');
});
