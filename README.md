# Snap Test Project

By: Abolfazl Isazadeh

## Installation

### .env

Copy the `.env.example` contents to `.env` file and fill its SMS providers API keys

### Composer

After cloning project, in project directory, run in terminal:

```
composer install
```

### Docker

Then for accessing project should run docker, so in project directory run in terminal:

```
docker-compose up -d --build
```

### Migrating and Seeding

Then for migrating project databases, in project directory run in terminal:

```
docker-compose exec php php artisan migrate
```

and for seeding databases run:

```
docker-compose exec php php artisan db:seed
```

### Queue Worker

At last for running queues in application, in project directory run in terminal:

```
docker-compose exec php php artisan queue:work
```

#### *Application will run on port 1401 by default.

[http://localhost:1401](http://localhost:1401)

## Usage

### Endpoints

Application has 2 endpoints:
<br>
Below endpoint will return most 3 transacted users and their transactions.
By default, it will return last 10 transactions of each user, you can change it with `count` query param.

```
[GET] http://localhost:1401/api/user/most-transacted?count=10
```

And
<br>
Below endpoint will store a new transaction by Card2Card method.

```
[POST] http://localhost:1401/api/transaction
```

Its request body should be like this json:

```json
{
    "from_card": "1234-1234-1234-1234",
    "to_card": "5678-5678-5678-5678",
    "amount": "10000"
}
```

## Description

### Adding new SMS provider

For adding new SMS provider, you can find a `sms.php` in project config folder. In this file, you can add your new sms provider. It should have a `class` property to 
declare its related class for service container. And you can add new provider's own authentication properties to this array.
For changing sms provider whole the system just change `SMS_PROVIDER` in `.env` file to your interested provider.
<br>
Notice that new SMS provider class should be implemented as `ISmsProvider` interface, and adding its own `send`
 method implementation from provider API documents.
